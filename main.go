package main

import (
	"twitch-clone/pkg/app"
)

func main() {
	app.Init()
}
