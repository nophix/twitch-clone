import { UserFollows } from '../types';

export const following: UserFollows = {
  total: 4,
  data: [
    {
      from_id: '171003792',
      from_login: 'niku',
      from_name: 'niku',
      to_id: '23161357',
      to_name: 'LIRIK',
      to_login: 'lirik',
      followed_at: '2017-08-22T22:55:24Z',
    },
    {
      from_id: '171003792',
      from_login: 'niku',
      from_name: 'niku',
      to_id: '23161358',
      to_name: 'Cowser',
      to_login: 'cowser',
      followed_at: '2017-08-22T22:55:24Z',
    },
  ],
  pagination: {
    cursor: 'eyJiIjpudWxsLCJhIjoiMTUwMzQ0MTc3NjQyNDQyMjAwMCJ9',
  },
};
