import { FollowedStreams } from '../types';

const streams: FollowedStreams = {
  data: [
    {
      id: '41375541868',
      user_id: '459331509',
      user_login: 'auronplay',
      user_name: 'auronplay',
      game_id: '494131',
      game_name: 'Little Nightmares',
      type: 'live',
      title: 'hablamos y le damos a Little Nightmares 1',
      viewer_count: 78365,
      started_at: '2021-03-10T15:04:21Z',
      language: 'es',
      thumbnail_url: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_auronplay-250x250.jpg',
      tag_ids: [],
      is_mature: false,
    },
    {
      id: '41375541869',
      user_id: '459331510',
      user_login: 'xqcow',
      user_name: 'xqcow',
      game_id: '494131',
      game_name: 'Just Chatting',
      type: 'live',
      title: 'slam',
      viewer_count: 56230,
      started_at: '2022-09-29T14:04:21Z',
      language: 'en',
      thumbnail_url: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_xqcow-250x250.jpg',
      tag_ids: [],
      is_mature: false,
    },
  ],
  pagination: {
    cursor:
      'eyJiIjp7IkN1cnNvciI6ImV5SnpJam8zT0RNMk5TNDBORFF4TlRjMU1UY3hOU3dpWkNJNlptRnNjMlVzSW5RaU9uUnlkV1Y5In0sImEiOnsiQ3Vyc29yIjoiZXlKeklqb3hOVGs0TkM0MU56RXhNekExTVRZNU1ESXNJbVFpT21aaGJITmxMQ0owSWpwMGNuVmxmUT09In19',
  },
};

export default streams;
