import { FC } from 'react';

const ChatBadge: FC = () => {
  return <span className="w-5 h-5 rounded-sm bg-pink-300 inline-block align-middle" />;
};

export default ChatBadge;
